var req = new XMLHttpRequest();
var receivedJSON = null;


function loadData ()
{
	hideView();

	req.onreadystatechange = function ()
	{
		if (req.readyState == 4)
		{
			if (req.status === 200 || // Normal http
				req.status === 0)
			{ // Chrome w/ --allow-file-access-from-files
				receivedData = req.responseText;
				receivedJSON = JSON.parse(receivedData);
				enableView();
				drawGraph(extractData(receivedJSON, 100));
			}
		}
	};

	req.open('GET', 'http://localhost:5555/getCsv', true);
	req.send(null);
}

var drawGraph = function (data)
{
	new Dygraph(document.getElementById("graph"),
		data, {
			rollPeriod: 1,
			legend: 'always',
			title: 'Speed Test',
			titleHeight: 32,
			ylabel: 'Mbit/s',
			xlabel: 'Date',
			labelsDivStyles: {
				'text-align': 'right',
				'background': 'none'
			},
			strokeWidth: 1.5,
			underlayCallback: function (canvas, area, g)
			{

				canvas.fillStyle = "rgba(255, 255, 102, 1.0)";

				function highlight_period(x_start, x_end)
				{
					var canvas_left_x = g.toDomXCoord(x_start);
					var canvas_right_x = g.toDomXCoord(x_end);
					var canvas_width = canvas_right_x - canvas_left_x;
					canvas.fillRect(canvas_left_x, area.y, canvas_width, area.h);
				}

				var min_data_x = g.getValue(0, 0);
				var max_data_x = g.getValue(g.numRows() - 1, 0);

				// get day of week
				var d = new Date(min_data_x);
				var dow = d.getUTCDay();

				var w = min_data_x;
				// starting on Sunday is a special case
				if (dow === 0)
				{
					highlight_period(w, w + 12 * 3600 * 1000);
				}
				// find first saturday
				while (dow != 6)
				{
					w += 24 * 3600 * 1000;
					d = new Date(w);
					dow = d.getUTCDay();
				}
				// shift back 1/2 day to center highlight around the point for the day
				w -= 12 * 3600 * 1000;
				while (w < max_data_x)
				{
					var start_x_highlight = w;
					var end_x_highlight = w + 2 * 24 * 3600 * 1000;
					// make sure we don't try to plot outside the graph
					if (start_x_highlight < min_data_x)
					{
						start_x_highlight = min_data_x;
					}
					if (end_x_highlight > max_data_x)
					{
						end_x_highlight = max_data_x;
					}
					highlight_period(start_x_highlight, end_x_highlight);
					// calculate start of highlight for next Saturday
					w += 7 * 24 * 3600 * 1000;
				}
			}
		});
};

var extractData = function (data, limit)
{
	var extractedData = "";

	console.log(data.length + " | " + limit);

	var newLimit = setView(limit, data.length);

//		console.log(newLimit);

	for (var i = (data.length - newLimit); i < data.length; i++)
	{
		var dateString = data[i]['date'];
		dateString = dateString.replace(',', '');


		var string = dateString + "," + data[i]['download'] + "," + data[i]['upload'] + "\n";
		extractedData += string;
	}

//		console.log(extractedData);

	return "num,download,upload\n" + extractedData;
};

var setView = function (percent, countOfData)
{
	return Math.round((countOfData / 100) * percent);
};

function enableView()
{
	$("#graph").show();
	$("#buttons").show();
	$("#loading").hide();

};

function hideView()
{
	$("#graph").hide();
	$("#buttons").hide();
	$("#loading").show();

};


