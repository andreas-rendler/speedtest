var express = require('express');
var http = require('http');
var fs = require('fs');
var url = require('url');

var Converter = require("csvtojson").Converter;

var app = express();

app.get('/speedtest', function (request, response)
{
	console.log("REST - speedtest");

	var pathname = "index.htm";

	// Read the requested file content from file system
	fs.readFile(pathname, function (err, data)
	{
		if (err)
		{
			// console.log(err);
			// HTTP Status: 404 : NOT FOUND
			// Content Type: text/plain
			response.writeHead(404, {'Content-Type': 'text/html'});
		}
		else
		{
			//Page found
			// HTTP Status: 200 : OK
			// Content Type: text/plain
			response.writeHead(200, {'Content-Type': 'text/html'});

			// Write the content of the file to response body
			response.write(data.toString());
		}
		// Send the response body
		response.end();

	});
});

app.get('/resource/:file_name', function (request, response)
{
	console.log("REST - resource -> " + request.params.file_name);

	// Read the requested file content from file system
	fs.readFile(request.params.file_name, function (err, data)
	{
		if (err)
		{
			// console.log(err);
			// HTTP Status: 404 : NOT FOUND
			// Content Type: text/plain
			response.writeHead(404, {'Content-Type': 'text/html'});
		}
		else
		{
			//Page found
			// HTTP Status: 200 : OK
			// Content Type: text/plain
			response.writeHead(200, {'Content-Type': 'text/html'});

			// Write the content of the file to response body
			response.write(data.toString());
		}
		// Send the response body
		response.end();

	});

});


app.get('/getCsv', function (request, response)
{
	console.log("REST - getCsv");

	var pathname = "speedtest.csv";


	// Read the requested file content from file system
	fs.readFile(pathname, function (err, data)
	{
		if (err)
		{
			console.log(err);
			// HTTP Status: 404 : NOT FOUND
			// Content Type: text/plain
			response.writeHead(404, {'Content-Type': 'text/html'});
		}
		else
		{
			var converter = new Converter({
				delimiter: ',',
				quote: '"'
			});

			var rawString = "date,download,upload,unit,host\n" + data.toString();

			converter.fromString(rawString, function (err, result)
			{

				// console.log(result);

				response.header("Content-Type", "application/json; charset=utf-8");
				response.send(result);
			});
		}


	});

});


var server = app.listen(5555, function ()
{

	var host = server.address().address;
	var port = server.address().port;

	console.log("Example app listening at http://%s:%s", host, port);

});